from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import json

import requests


def Get_Picture_URL(city, state):  # returns a picture URL via Plexes API
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state
    }

    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}



def Get_Weather(city, state):
    country_code = 'US' # hard coded, use case only requres US location
    limit = 1 # hard coded, we only want the lat/long of 1 location
    url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{country_code}&limit={limit}&appid={OPEN_WEATHER_API_KEY}'
    ST = str(state) # concatenating this directly was not not working due to added space
    q = city + ',' + ST + ',US'
    print(q)
    param = {
        'q' : q,
        'appid' : OPEN_WEATHER_API_KEY,
        'limit' : 1
    }
    response = requests.get(url, params = param)
    content = json.loads(response.content)

    lat = content[0]['lat']
    lon = content[0]['lon']

    url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}'

    response = requests.get(url)
    weathercontent = json.loads(response.content)
    temperature = weathercontent['main']['temp']
    description = weathercontent['weather'][0]['description']

    temperature = ((temperature - 273.15)*1.8) + 32.00
    temperature = int(temperature)
    temperature = str(temperature)+ "f"

    return {"temp": temperature,
            "description": description}
