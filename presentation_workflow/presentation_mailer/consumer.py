import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_approval(body):
    print(body)
    content = json.loads(body)
    print(content)
    
    Subject = "Your presentation has been accepted"
    name = content['Presenter_name']
    title = content['Queue_Title']
    Body = f"{name}, we're happy to tell you that your presentation {title} has been accepted."

    send_mail(
        Subject,
        Body,
        'admin@conference.go',
        [content['Presenter_email']],
        fail_silently=False,
        )


def process_rejection(body):
    print(body)
    content = json.loads(body)
    print(content)
    
    Subject = "Your presentation has been rejected"
    name = content['Presenter_name']
    title = content['Queue_Title']
    Body = f"{name}, we're sorry to tell you that your presentation {title} has been rejected."

    send_mail(
        Subject,
        Body,
        'admin@conference.go',
        [content['Presenter_email']],
        fail_silently=False,
        )


parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='presentation_approvals')
channel.basic_consume(
    queue='presentation_approvals',
    on_message_callback=process_approval, # talking to the callback function above
    auto_ack=True,
)
channel.queue_declare(queue='presentation_rejections')
channel.basic_consume(
    queue='presentation_rejections',
    on_message_callback=process_rejection, # talking to the callback function above
    auto_ack=True,
)
channel.start_consuming()

