from json import JSONEncoder

from datetime import datetime

from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        # print("o is: ", o, " and it is data type: ", type(o))
        # print("self.model is: ", self.model, " and it is data type: ", type(self.model))
        if isinstance(o, self.model):
            # print("We are using this encoder")
            d = {}
            if hasattr(o, 'get_api_url'):
                d['href'] = o.get_api_url()

            for property in self.properties: # this is pulling the data we want out from the big messy Json file
                value = getattr(o, property) # instead of having to manually pull it out with name[nest][nest][nest]
                if property in self.encoders:   # figure out what these are doing
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d.update(self.get_extra_data(o)) # edge case fix part 1
                d[property] = value
            return d
        else:
            # print("We are using a different default method")
            return super().default(o)  # From the documentation

    def get_extra_data(self, o):
        return {}
