from django.http import JsonResponse

from .models import Attendee, ConferenceVO

from common.json import ModelEncoder

# from events.api_views import ConferenceListEncoder
    # this import no longer works after the refactor

# from events.models import Conference
from .models import AccountVO  # replacing the above import

from django.views.decorators.http import require_http_methods

import json


# there seems to have been a type, learn called this ConferenceVODetailEncoder
# even though it is replaceing ConferenceListEncoder
# so I'm caling it list encoder and watching the href property for errors
class ConferenceVOListEncoder(ModelEncoder): 
    model = ConferenceVO
    properties = ["name", "import_href"]  # if this throws errors, delete href


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'name',
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):  # added =none and _vo_
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeesListEncoder,
        )
    else:
        content = json.loads(request.body)
        # Get the Conference object and put it in the content dict
        try:
            # added line
            conference_href = content['conference']
            
            # conference = Conference.objects.get(id=conference_vo_id)
            conference = ConferenceVO.objects.get(import_href=conference_href)
            
            content["conference"] = conference

        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


    # response = []
    # attendees = Attendee.objects.all()

    # for attendee in attendees:
    #     response.append(
    #         {
    #             "name": attendee.name,
    #             "href": attendee.get_api_url(),
    #         }
    #     )

    # return JsonResponse({'attendees': response})


# class ConferenceEncoder(ModelEncoder):
#     model = Location
#     properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'name',
        "email",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVOListEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()


@require_http_methods(["GET","DELETE","PUT"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            {"attendee": attendee},
            encoder=AttendeeDetailEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(request.body)
        # new code
        Attendee.objects.filter(id=pk).update(**content)

        # copied from get detail
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.get_api_url(),
    #         }
    #     }
    # )
